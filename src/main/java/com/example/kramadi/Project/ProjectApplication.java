package com.example.kramadi.Project;

import com.example.kramadi.Project.Controller.Controller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectApplication {
	private static final Logger LOG = LoggerFactory.getLogger("JCG");

	@Autowired
	private Controller controller;

	public static void main(String[] args) {
		SpringApplication.run(ProjectApplication.class, args);
	}

}
