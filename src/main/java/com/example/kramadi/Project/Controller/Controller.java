package com.example.kramadi.Project.Controller;

import java.util.List;
import java.util.UUID;

import com.example.kramadi.Project.Model.Specialist;
import com.example.kramadi.Project.Model.TimeTable;
import com.example.kramadi.Project.Service.SpecialistService;
import com.example.kramadi.Project.Service.TimeTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/specialist")
public class Controller {
    @Autowired
    private SpecialistService specialistService;

    @Autowired
    private TimeTableService timeTableService;

    @PostMapping
    public ResponseEntity<Specialist> createSpecialist(@RequestBody Specialist specialist) {
        Specialist createdSpecialist = specialistService.createSpecialist(specialist);
        return new ResponseEntity<>(createdSpecialist, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Specialist>> getAllSpecialist() {
        List<Specialist> specialists = specialistService.readAllSpecialist();
        if (specialists.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(specialists, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Specialist> updateSpecialist(@PathVariable("id") UUID id, @RequestBody Specialist specialist) {
        Specialist updatedSpecialist = specialistService.updateSpecialist(id, specialist);
        return new ResponseEntity<>(updatedSpecialist, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<HttpStatus> deleteSpecialist(@PathVariable("id") UUID id) {
        specialistService.deleteSpecialist(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/{specialistId}/timetable")
    public ResponseEntity<TimeTable> createTimeTable(@PathVariable(value = "specialistId") UUID specialistId,
                                                   @RequestBody TimeTable timeTables) {
        TimeTable createdTimeTable = timeTableService.createTimeTable(specialistId, timeTables);
        return new ResponseEntity<>(createdTimeTable, HttpStatus.CREATED);
    }

    @GetMapping("/{specialistId}/timetable")
    public ResponseEntity<List<TimeTable>> getAllTimeTablesBySpecialistId(@PathVariable(value = "specialistId") UUID specialistId) {
        List<TimeTable> timeTableList = timeTableService.readTimeTable(specialistId);
        return new ResponseEntity<>(timeTableList, HttpStatus.OK);
    }

    @PutMapping("/timetable/{id}")
    public ResponseEntity<TimeTable> updateTimeTable(@PathVariable("id") long id, @RequestBody TimeTable timetableRequest) {
        TimeTable updatedTimeTable = timeTableService.updateTimeTable(id,timetableRequest);
        return new ResponseEntity<>(updatedTimeTable ,HttpStatus.OK);
    }

    @DeleteMapping("/timetable/{id}")
    public ResponseEntity<HttpStatus> deleteTimeTable(@PathVariable("id") long id) {
        timeTableService.deleteTimeTable(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{specialistId}/TimeTables")
    public ResponseEntity<List<TimeTable>> deleteAllTimeTablesOfSpecialist(@PathVariable(value = "specialistId") UUID specialistId) {
        timeTableService.deleteAllTimeTablesOfSpecialist(specialistId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
