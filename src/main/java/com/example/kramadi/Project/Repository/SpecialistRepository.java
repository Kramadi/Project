package com.example.kramadi.Project.Repository;

import java.util.UUID;

import com.example.kramadi.Project.Model.Specialist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialistRepository extends JpaRepository<Specialist, UUID> {
}