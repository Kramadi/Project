package com.example.kramadi.Project.Repository;

import java.util.List;
import java.util.UUID;

import com.example.kramadi.Project.Model.TimeTable;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeTableRepository extends JpaRepository<TimeTable, Long> {
    List<TimeTable> findBySpecialistId(UUID specialistId);

    @Transactional
    void deleteBySpecialistId(UUID specialistId);
}
