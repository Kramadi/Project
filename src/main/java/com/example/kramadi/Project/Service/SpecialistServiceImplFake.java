package com.example.kramadi.Project.Service;

import com.example.kramadi.Project.Exception.ResourceNotFoundException;
import com.example.kramadi.Project.Model.Specialist;

import java.util.*;

public class SpecialistServiceImplFake implements SpecialistService{

    public final Map<UUID, Specialist> mapSpecialist = new HashMap<>();

    @Override
    public List<Specialist> readAllSpecialist() {
        return new ArrayList<>(mapSpecialist.values());
    }

    @Override
    public Specialist readSpecialist(UUID id) {
        return mapSpecialist.get(id);
    }

    @Override
    public Specialist createSpecialist(Specialist specialist) {
        UUID uuid = UUID.randomUUID();
        mapSpecialist.put(uuid, specialist);
        return specialist;
    }

    @Override
    public Specialist updateSpecialist(UUID id, Specialist specialist) {
        if(mapSpecialist.containsKey(id)){
        mapSpecialist.put(id, specialist);
        }else {
            throw new ResourceNotFoundException("Not found Specialist with id = " + id);
        }
        return specialist;
    }

    @Override
    public void deleteSpecialist(UUID id) {
        mapSpecialist.remove(id);
    }
}
