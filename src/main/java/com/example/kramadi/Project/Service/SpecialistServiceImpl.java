package com.example.kramadi.Project.Service;

import com.example.kramadi.Project.Exception.ResourceNotFoundException;
import com.example.kramadi.Project.Model.Specialist;
import com.example.kramadi.Project.Repository.SpecialistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class SpecialistServiceImpl implements SpecialistService {

    @Autowired
    private SpecialistRepository specialistRepository;

    public SpecialistServiceImpl(){}

    @Override
    public List<Specialist> readAllSpecialist() {
        return this.specialistRepository.findAll();
    }

    @Override
    public Specialist readSpecialist(UUID id) {
        return specialistRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Not found Specialist with id = " + id));
    }

    @Override
    public Specialist createSpecialist(Specialist specialist) {
        specialist.setId(UUID.randomUUID());
        this.specialistRepository.save(specialist);
        return specialist;
    }

    @Override
    public Specialist updateSpecialist(UUID id, Specialist specialist) {
        this.specialistRepository.findById(id).orElseThrow(
                () -> new ResourceNotFoundException("Not found Specialist with id = " + id));
        specialist.setId(id);
        this.specialistRepository.save(specialist);
        return specialist;
    }

    @Override
    public void deleteSpecialist(UUID id) {
        this.specialistRepository.deleteById(id);
    }
}
