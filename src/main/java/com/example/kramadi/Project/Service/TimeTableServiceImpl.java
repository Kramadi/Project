package com.example.kramadi.Project.Service;

import com.example.kramadi.Project.Exception.ResourceNotFoundException;
import com.example.kramadi.Project.Model.TimeTable;
import com.example.kramadi.Project.Repository.SpecialistRepository;
import com.example.kramadi.Project.Repository.TimeTableRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class TimeTableServiceImpl implements TimeTableService{

    @Autowired
    TimeTableRepository timeTableRepository;

    @Autowired
    SpecialistRepository specialistRepository;

    TimeTableServiceImpl(){}

    @Override
    public List<TimeTable> readTimeTable(UUID specialistId) {
        if (!specialistRepository.existsById(specialistId)) {
            throw new ResourceNotFoundException("Not found Specialist with id = " + specialistId);
        }
        return timeTableRepository.findBySpecialistId(specialistId);
    }

    @Override
    public List<TimeTable> readAllTimeTables() {
        return timeTableRepository.findAll();
    }

    @Override
    public TimeTable createTimeTable(UUID specialistId, TimeTable timeTable) {
        specialistRepository.findById(specialistId).map(specialist -> {
            if(timeTableRepository.findBySpecialistId(specialistId).stream().count() < 5) {
                timeTable.setSpecialist(specialist);
                return timeTableRepository.save(timeTable);
            }else {
                throw new ResourceNotFoundException("TimeTable is full");
            }
        }).orElseThrow(() -> new ResourceNotFoundException("Not found Specialist with id = " + specialistId));
        return timeTable;
    }

    @Override
    public TimeTable updateTimeTable(Long timeSlotId, TimeTable timeTable) {
        TimeTable timeTableFromDb = timeTableRepository.findById(timeSlotId).orElseThrow(
                () -> new ResourceNotFoundException("SlotId " + timeSlotId + " not found"));
        timeTable.setSpecialist(timeTableFromDb.getSpecialist());
        timeTable.setSlotId(timeSlotId);
        this.timeTableRepository.save(timeTable);
        return timeTable;
    }

    @Override
    public void deleteTimeTable(Long timeSlotId) {
        this.timeTableRepository.deleteById(timeSlotId);
    }

    @Override
    public void deleteAllTimeTablesOfSpecialist(UUID id) {
        if (!specialistRepository.existsById(id)) {
            throw new ResourceNotFoundException("Not found Specialist with id = " + id);
        }
        timeTableRepository.deleteBySpecialistId(id);
    }
}
