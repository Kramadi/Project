package com.example.kramadi.Project.Service;

import com.example.kramadi.Project.Model.Specialist;

import java.util.List;
import java.util.UUID;

public interface SpecialistService {

    List<Specialist> readAllSpecialist();

    Specialist readSpecialist(UUID id);

    Specialist createSpecialist(Specialist specialist);

    Specialist updateSpecialist(UUID id, Specialist specialist);

    void deleteSpecialist (UUID id);
}
