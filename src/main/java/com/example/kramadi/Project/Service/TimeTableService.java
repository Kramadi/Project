package com.example.kramadi.Project.Service;

import com.example.kramadi.Project.Model.TimeTable;

import java.util.List;
import java.util.UUID;

public interface TimeTableService {
    List<TimeTable> readTimeTable(UUID specialistId);

    List<TimeTable> readAllTimeTables();

    TimeTable createTimeTable(UUID specialistId, TimeTable timeTable);

    TimeTable updateTimeTable(Long timeSlotId, TimeTable updatedTimeSlot);

    void deleteTimeTable (Long timeSlotId);

    void deleteAllTimeTablesOfSpecialist(UUID id);
}
