package com.example.kramadi.Project.Service;

import com.example.kramadi.Project.Model.Specialist;
import com.example.kramadi.Project.Model.TimeTable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class TimeTableServiceImplFake implements TimeTableService {

    private final Map<Long, TimeTable> mapTimeTable = new HashMap<>();

    SpecialistServiceImplFake specialistServiceImplFake;

    Specialist specialist = new Specialist();

    @Override
    public List<TimeTable> readTimeTable(UUID specialistId) {

        return mapTimeTable.values().stream().filter(timeTable -> timeTable.getSpecialist().equals(specialistId)).toList();
    }

    @Override
    public List<TimeTable> readAllTimeTables() {
        return mapTimeTable.values().stream().toList();
    }

    @Override
    public TimeTable createTimeTable(UUID specialistId, TimeTable timeTable) {
            specialist.setId(specialistId);
            timeTable.setSpecialist(specialist);
            mapTimeTable.put(timeTable.getSlotId(), timeTable);
            return timeTable;
    }

    @Override
    public TimeTable updateTimeTable(Long timeSlotId, TimeTable updatedTimeSlot) {
        mapTimeTable.put(timeSlotId, updatedTimeSlot);
        return updatedTimeSlot;
    }

    @Override
    public void deleteTimeTable(Long timeSlotId) {
        mapTimeTable.remove(timeSlotId);
    }

    @Override
    public void deleteAllTimeTablesOfSpecialist(UUID id) {
        specialistServiceImplFake.readSpecialist(id);
        mapTimeTable.values().stream().filter(timeTable -> timeTable.getSpecialist().equals(id)).forEach(mapTimeTable::remove);
    }
}
