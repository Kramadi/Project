package com.example.kramadi.Project.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import java.util.UUID;

@Entity
public class Specialist {
    @Id
    @GeneratedValue
    @JsonIgnore
    private UUID id;
    private String name;

    public Specialist(String name) {
        this.name = name;
    }

    public Specialist() {
    }

    public UUID getId() {
        return this.id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return String.format("Specialist{name='%s'}", this.name);
    }
}
