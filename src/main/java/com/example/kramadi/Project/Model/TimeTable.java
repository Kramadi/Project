package com.example.kramadi.Project.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class TimeTable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long SlotId;
    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private String bookedBy;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "specialistId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private Specialist specialist;

    public TimeTable(String bookedBy, LocalDateTime fromDate, LocalDateTime toDate) {
        this.bookedBy = bookedBy;
        this.fromDate = fromDate;
        this.toDate = toDate;
    }

    public TimeTable() {}

    public Long getSlotId() {
        return this.SlotId;
    }

    public void setSlotId(Long slotId) {
        this.SlotId = slotId;
    }

    public LocalDateTime getFromDate() {
        return this.fromDate;
    }

    public void setFromDate(LocalDateTime from) {
        this.fromDate = from;
    }

    public LocalDateTime getToDate() {
        return this.toDate;
    }

    public void setToDate(LocalDateTime to) {
        this.toDate = to;
    }

    public String getBookedBy() {
        return this.bookedBy;
    }

    public void setBookedBy(String bookedBy) {
        this.bookedBy = bookedBy;
    }

    public Specialist getSpecialist(){
        return specialist;
    }

    public void setSpecialist(Specialist specialist){
        this.specialist = specialist;
    }

    public String toString() {
        return String.format("TimeTable{SlotId='%s', from=%s, to=%s, bookedBy=%s}", this.SlotId, this.fromDate, this.toDate, this.bookedBy);
    }
}
